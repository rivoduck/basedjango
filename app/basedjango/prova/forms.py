from django.forms import ModelForm
from prova.models import Author

class ModificaAutori(ModelForm):
    class Meta:
        model=Author
        fields=['name', 'email']
