
from django.db import models
from django.utils import timezone

class Author(models.Model):
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=255)
    def __str__(self):
        return self.name+' ('+self.email+')'

class Article(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField(max_length=65535)
    pub_date = models.DateTimeField('date published', auto_now_add=True)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
    def __str__(self):
        return self.title+' (Pubblicazione: '+str(self.pub_date)+' , Autore: '+str(self.author)+')'

class Comment(models.Model):
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=255)
    comment = models.CharField(max_length=1000)
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    def __str__(self):
        return self.comment+' (Nome ed e-mail commento: '+self.name+' , '+self.email+' , Articolo di provenienza:  '+str(self.article)+')'
