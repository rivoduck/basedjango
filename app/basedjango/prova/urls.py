from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('detail', views.detail, name='detail'),
    path('detail/<int:year>/', views.detail),
    path('detail/<int:year>/<int:month>/', views.detail),

    path('authors', views.authors, name='authors'),
    path('articles', views.articles, name='articles'),
    path('articles/<int:authorid>/', views.articles, name='articles'),
    path('text_comments', views.text_comments, name='text_comments'),
    path('text_comments/<int:articleid>/', views.text_comments, name='text_comments'),

    path('modifiche', views.modifiche, name='modifiche'),
    path('modifiche/<int:authorid>/', views.modifiche, name='modifiche'),
    path('elimina/<int:authorid>/', views.elimina, name='elimina'),

]
