
from django.utils import timezone
from .models import Author, Article, Comment
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from .forms import ModificaAutori



import logging
logger = logging.getLogger(__name__)

def index(request):
    return render(request, 'prova/index.html')

def detail(request, year=None, month=None):

    now = timezone.now()
    c_year=2017
    c_month=12
    nomi=['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre']
    if not year :
        year = c_year
    if not month :
        month = c_month

    error=''
    if (year>c_year):
        error='In che anno credi di essere?'
    if (month<1 or month>12):
        error='In che mese credi di essere?'
    if (year>c_year) and (month<1 or month>12):
        error='Non hai proprio idea di dove vivi eh?'

    if (error!=''):
        context = {'error': error}
        return render(request, 'prova/error.html', context)

    else:
        context = {'year': year, 'month': nomi[month-1], 'hour': now}
        return render(request, 'prova/date.html', context)


def authors(request):
    a=Author.objects.all()
    titolo='Tutti gli autori'
    context = {'authors': a, 'titolo':titolo}
    return render(request, 'prova/authors.html', context)

def articles(request, authorid=None):
    autore=None
    if authorid !=None:
        autore = Author.objects.get(pk=authorid)
        articoli = autore.article_set.all()
        titolo='Articles of '+ autore.name
    else :
        articoli = Article.objects.all()
        titolo='Tutti gli articoli'


    context = {'articles': articoli, 'autore': autore, 'titolo': titolo}
    return render(request, 'prova/articles.html', context)

def text_comments(request, articleid):
    articolo = Article.objects.get(pk=articleid)
    commenti = articolo.comment_set.all()
    titolo='Text and Comments of '+ articolo.title
    context = {'text_comments': commenti, 'articolo': articolo, 'titolo':titolo}
    return render(request, 'prova/text_comments.html', context)

def modifiche(request, authorid=None):
    autore = None
    if authorid != None:
        try:
            autore = Author.objects.get(pk=authorid)
        except:
            context={'id':authorid}
            return render(request, 'prova/error.html', context)

    if request.method == 'POST':
        #se chiamo con POST
        form = ModificaAutori(request.POST, instance=autore)
        # l'instance lega la request all'id dell'autore e lega le modifiche all'autore
        # su cui sono state effettuate

        if form.is_valid():
            logger.debug('il form e valido')
            form.save()
            # Validatura form
            return HttpResponseRedirect('/authors')

    else:
        #se chiamo con GET
        if autore!=None:
            campoform = ModificaAutori(instance=autore)
        else:
            campoform = ModificaAutori()

    return render(request, 'prova/form_modifica.html', {'form': campoform})





def elimina(request, authorid):
    try:
        autore = Author.objects.get(pk=authorid)
    except:
        context = {'id':authorid}
        return render(request, 'prova/error.html', context)

    if request.method == 'POST':
        logger.debug(request.POST)
        if 'conferma' in request.POST and request.POST['conferma']=='cancella':
            autore.delete()

        return HttpResponseRedirect('/authors')

    else:
        context={'autore': autore}
        return render(request, 'prova/form_elimina.html', context)

