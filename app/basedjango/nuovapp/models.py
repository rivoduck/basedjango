from django.db import models


class Camere(models.Model):
    id = models.CharField(max_length=10)
    piano = models.CharField(max_length=2)
    tipo = models.CharField(max_length=255)
    aggletti = models.CharField(max_length=1)
    optionals = models.CharField(max_length=255)
    def __str__(self):
        return 'camera '+self.tipo+' (che si trova al piano '+self.piano+' con un aggiunta di '+self.aggletti+' e con optionals '+self.optionals+')'

class Clienti(models.Model):
    id = models.CharField(max_length=10)
    nome = models.CharFliend(max_length=100)
    telefono = models.CharField(max_length=10)
    def __str__(self):
        return 'cliente '+self.nome+' (telefono: '+self.telefono+')'
