from django.contrib import admin

from .models import Camere

admin.site.register(Camere)
admin.site.register(Clienti)
