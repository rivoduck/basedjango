from django.urls import path

from . import views

urlpatterns = [
    path('', views.inizio, name='inizio'),
    path('camere', views.camere, name='camere'),
    path('clienti', views.clienti, name='clienti'),

]
