
from .models import Camere, Clienti
from django.shortcuts import render
from .forms import ModificaCamere, ModificaClienti

import logging
logger = logging.getLogger(__name__)

def inizio(request):
    return render(request, 'nuovapp/inizio.html')

def camere(request):
    a=Camere.objects.all()
    titolo='Tutte le camere'
    context = {'camere': a, 'titolo':titolo}
    return render(request, 'nuovapp/camere.html', context)

def clienti(request):
    a=Clienti.objects.all()
    titolo='Tutti i clienti'
    context = {'clienti': a, 'titolo':titolo}
    return render(request, 'nuovapp/clienti.html', context)
