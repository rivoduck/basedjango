from django.forms import ModelForm
from prova.models import Clienti, Camere

class ModificaCamere(ModelForm):
    class Meta:
        model=Camere
        fields=['id', 'piano', 'tipo', 'aggletti', 'optionals']

class ModificaClienti(ModelForm):
    class Meta:
        model=Clienti
        fields=['id', 'nome', 'telefono']
